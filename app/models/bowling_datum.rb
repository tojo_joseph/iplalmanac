class BowlingDatum < ActiveRecord::Base
    belongs_to :match_datum
    
    #player profile scopes
    scope :player_total_overs_integer_part_by_year   , -> (player_name){where("bowler_name= ?",player_name).group("year").sum("(cast(overs*10 as integer)%10)")}
    scope :player_total_overs_integer_part           , -> (player_name){where("bowler_name= ?",player_name).sum("(cast(overs*10 as integer)%10)")}
    scope :player_total_overs_decimal_part_by_year   , -> (player_name){where("bowler_name= ?",player_name).group("year").sum("floor(overs)*6")}
    scope :player_total_overs_decimal_part           , -> (player_name){where("bowler_name= ?",player_name).sum("floor(overs)*6")}
    scope :player_total_runs_given_by_year           , -> (player_name){where("bowler_name= ?",player_name).group("year").sum("runs_given")}
    scope :player_total_runs_given                   , -> (player_name){where("bowler_name= ?",player_name).sum("runs_given")}
    scope :player_total_wickets_by_year              , -> (player_name){where("bowler_name= ?",player_name).group("year").sum("wickets")}
    scope :player_total_wickets                      , -> (player_name){where("bowler_name= ?",player_name).sum("wickets")}
    scope :player_total_4W_haul_by_year              , -> (player_name){where("bowler_name=? AND wickets=?",player_name,"4").group("year").count("id")}
    scope :player_total_4W_haul                      , -> (player_name){where("bowler_name=? AND wickets=?",player_name,"4").count("id")}
    scope :player_total_5W_haul_by_year              , -> (player_name){where("bowler_name=? AND wickets=?",player_name,"5").group("year").count("id")}
    scope :player_total_5W_haul                      , -> (player_name){where("bowler_name=? AND wickets=?",player_name,"5").count("id")}

end