class MatchDatum < ActiveRecord::Base
    has_many :batting_data
    has_many :bowling_data
    
    #List of matches with filter
    scope :list_of_matches                   , -> (year_filter,team_filter) {select("*")
                                                  .order("year DESC").order("match_number DESC")
                                                  .where("cast(year as text)  LIKE ? AND (team_a_name LIKE ? OR team_b_name LIKE ?)",year_filter,team_filter,team_filter)}
    #Scorecard for particular match
    scope :batting_scorecard_first_innings   , -> (id) {find(id).batting_data.where("innings= ?","1")}
    scope :batting_scorecard_second_innings  , -> (id) {find(id).batting_data.where("innings= ?","2")}
    scope :bowling_scorecard_first_innings   , -> (id) {find(id).bowling_data.where("innings= ?","1")} 
    scope :bowling_scorecard_second_innings  , -> (id) {find(id).bowling_data.where("innings= ?","2")}
    scope :match_data                        , -> (id) {find(id)}
    
    #For landing page display 
    scope :total_seasons                     , -> {select("year").distinct.count}
    scope :total_matches                     , -> {count("id")}
    scope :total_runs_first_innings          , -> {sum("team_a_score")} 
    scope :total_runs_second_innings         , -> {sum("team_b_score")}
    scope :total_wickets_first_innings       , -> {sum("team_a_wickets")}
    scope :total_wickets_second_innings      , -> {sum("team_b_wickets")}
    
end
