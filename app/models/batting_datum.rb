class BattingDatum < ActiveRecord::Base
    belongs_to :match_datum
    
    scope :total_sixes   , ->  {sum("sixes")}
    scope :total_fours   , ->  {sum("fours")}
    
    #player profile scopes
    scope :player_total_matches_by_year        , -> (player_name){where("batsmen_name= ?",player_name).group("year").count("id")}
    scope :player_total_matches                , -> (player_name){where("batsmen_name= ?",player_name).count("id")}   
    scope :player_total_not_out_by_year        , -> (player_name){where("batsmen_name= ? AND dismissal LIKE ?",player_name,"%NOT OUT%").group("year").count("id")} 
    scope :player_total_not_out                , -> (player_name){where("batsmen_name= ? AND dismissal LIKE ?",player_name,"%NOT OUT%").count("id")} 
    scope :player_total_runs_by_year           , -> (player_name){where("batsmen_name= ?",player_name).group("year").sum("runs")}
    scope :player_total_runs                   , -> (player_name){where("batsmen_name= ?",player_name).sum("runs")}    
    scope :player_HS_by_year                   , -> (player_name){where("batsmen_name= ?",player_name).group("year").maximum("runs")}
    scope :player_HS                           , -> (player_name){where("batsmen_name= ?",player_name).maximum("runs")}
    scope :player_total_balls_faced_by_year    , -> (player_name){where("batsmen_name= ?",player_name).group("year").sum("balls_faced")}
    scope :player_total_balls_faced            , -> (player_name){where("batsmen_name= ?",player_name).sum("balls_faced")}
    scope :player_total_half_centuries_by_year , -> (player_name){where("batsmen_name= ? AND runs BETWEEN ? AND ?",player_name,"50","99").group("year").count("id")}
    scope :player_total_half_centuries         , -> (player_name){where("batsmen_name= ? AND runs BETWEEN ? AND ?",player_name,"50","99").count("id")}
    scope :player_total_centuries_by_year      , -> (player_name){where("batsmen_name= ? AND runs BETWEEN ? AND ?",player_name,"100","200").group("year").count("id")}
    scope :player_total_centuries              , -> (player_name){where("batsmen_name= ? AND runs BETWEEN ? AND ?",player_name,"100","200").count("id")}
    scope :player_total_sixes_by_year          , -> (player_name){where("batsmen_name= ?",player_name).group("year").sum("sixes")}
    scope :player_total_sixes                  , -> (player_name){where("batsmen_name= ?",player_name).sum("sixes")}
    scope :player_total_fours_by_year          , -> (player_name){where("batsmen_name= ?",player_name).group("year").sum("fours")}
    scope :player_total_fours                  , -> (player_name){where("batsmen_name= ?",player_name).sum("fours")}
    scope :player_total_catches_by_year        , -> (player_name){where("dismissal LIKE ?",player_name).group("year").count("id")}
    scope :player_total_catches                , -> (player_name){where("dismissal LIKE ?",player_name).count("id")}
    scope :player_total_stumpings_by_year      , -> (player_name){where("dismissal LIKE ?",player_name).group("year").count("id")}
    scope :player_total_stumpings              , -> (player_name){where("dismissal LIKE ?",player_name).count("id")}
    
end
