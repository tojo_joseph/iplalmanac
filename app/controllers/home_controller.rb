class HomeController < ApplicationController

    def welcome
        
        @total_seasons     = MatchDatum.total_seasons
        @total_matches     = MatchDatum.total_matches
        @total_runs        = MatchDatum.total_runs_first_innings + MatchDatum.total_runs_second_innings
        @total_wickets     = MatchDatum.total_wickets_first_innings + MatchDatum.total_wickets_second_innings
        @total_sixes       = BattingDatum.total_sixes
        @total_fours       = BattingDatum.total_fours
        
    
    end
end
