class MatchesController < ApplicationController
  def index
    
    
    if params[:year_filter].present?
      @match_list = MatchDatum.list_of_matches(params[:year_filter],params[:team_filter])
    else
      @match_list = MatchDatum.list_of_matches("2016","%%") 
    end
    
    respond_to do |format|
      format.html
      format.json
      format.js
    end
  
  end
  
  def scorecard
    #Get the first innings and second innings data separatley for easy dispaly in views.
    @batting_scorecard_first_innings  = MatchDatum.batting_scorecard_first_innings(params[:id])
    @batting_scorecard_second_innings = MatchDatum.batting_scorecard_second_innings(params[:id])
    @bowling_scorecard_first_innings  = MatchDatum.bowling_scorecard_first_innings(params[:id])
    @bowling_scorecard_second_innings = MatchDatum.bowling_scorecard_second_innings(params[:id])
    @match_data                       = MatchDatum.match_data(params[:id])
    
    
    #team name for writing on score card
    @first_innings                    = @batting_scorecard_first_innings[0].team
    @second_innings                   = @batting_scorecard_second_innings[0].team
  end
end
