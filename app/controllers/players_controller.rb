class PlayersController < ApplicationController
 
 def profile
   
   # Batting anf Fielding
   player_name_for_catches  = "%c #{params[:player_name]}%"
   player_name_for_stumping = "%st #{params[:player_name]}%"
   @player_total_matches_by_year        = BattingDatum.player_total_matches_by_year(params[:player_name])
   @player_total_matches                = BattingDatum.player_total_matches(params[:player_name])
   @player_total_not_out_by_year        = BattingDatum.player_total_not_out_by_year(params[:player_name])
   @player_total_not_out                = BattingDatum.player_total_not_out(params[:player_name])
   @player_total_runs_by_year           = BattingDatum.player_total_runs_by_year(params[:player_name])
   @player_total_runs                   = BattingDatum.player_total_runs(params[:player_name])
   @player_HS_by_year                   = BattingDatum.player_HS_by_year(params[:player_name])
   @player_HS                           = BattingDatum.player_HS(params[:player_name])
   @player_total_balls_faced_by_year    = BattingDatum.player_total_balls_faced_by_year(params[:player_name])
   @player_total_balls_faced            = BattingDatum.player_total_balls_faced(params[:player_name])
   @player_total_half_centuries_by_year = BattingDatum.player_total_half_centuries_by_year(params[:player_name])
   @player_total_half_centuries         = BattingDatum.player_total_half_centuries(params[:player_name])
   @player_total_centuries_by_year      = BattingDatum.player_total_centuries_by_year(params[:player_name])
   @player_total_centuries              = BattingDatum.player_total_centuries(params[:player_name])
   @player_total_sixes_by_year          = BattingDatum.player_total_sixes_by_year(params[:player_name])
   @player_total_sixes                  = BattingDatum.player_total_sixes(params[:player_name])
   @player_total_fours_by_year          = BattingDatum.player_total_fours_by_year(params[:player_name])
   @player_total_fours                  = BattingDatum.player_total_fours(params[:player_name])
   @player_total_catches_by_year        = BattingDatum.player_total_catches_by_year(player_name_for_catches)
   @player_total_catches                = BattingDatum.player_total_catches(player_name_for_catches)
   @player_total_stumpings_by_year      = BattingDatum.player_total_stumpings_by_year(player_name_for_stumping)
   @player_total_stumpings              = BattingDatum.player_total_stumpings(player_name_for_stumping)
   
   #Bowling
   @player_total_overs_decimal_part_by_year  = BowlingDatum.player_total_overs_decimal_part_by_year(params[:player_name])
   @player_total_overs_decimal_part          = BowlingDatum.player_total_overs_decimal_part(params[:player_name])
   @player_total_overs_integer_part_by_year  = BowlingDatum.player_total_overs_integer_part_by_year(params[:player_name])
   @player_total_overs_integer_part          = BowlingDatum.player_total_overs_integer_part(params[:player_name])
   @player_total_runs_given_by_year          = BowlingDatum.player_total_runs_given_by_year(params[:player_name])
   @player_total_runs_given                  = BowlingDatum.player_total_runs_given(params[:player_name])
   @player_total_wickets_by_year             = BowlingDatum.player_total_wickets_by_year(params[:player_name])
   @player_total_wickets                     = BowlingDatum.player_total_wickets(params[:player_name])
   @player_total_4W_haul_by_year             = BowlingDatum.player_total_4W_haul_by_year(params[:player_name])
   @player_total_4W_haul                     = BowlingDatum.player_total_4W_haul(params[:player_name])
   @player_total_5W_haul_by_year             = BowlingDatum.player_total_5W_haul_by_year(params[:player_name])
   @player_total_5W_haul                     = BowlingDatum.player_total_5W_haul(params[:player_name])

   
   
   
   
   
   
   
   
   
   
 end


end
