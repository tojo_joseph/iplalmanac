module MatchesHelper

    def if_a_equals_b?
        return @match_data.team_a_name.eql?(@batting_scorecard_first_innings[0].team)
    end
end
