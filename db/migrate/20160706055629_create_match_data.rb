class CreateMatchData < ActiveRecord::Migration
  def change
  
    create_table :match_data do |t|
      t.string  "super_over",                limit: 10,  null: false
      t.string  "team_a_name",               limit: 100, null: false
      t.integer "team_a_super_over_score",   limit: 4
      t.integer "team_a_super_over_wickets", limit: 4
      t.integer "team_a_score",              limit: 4
      t.integer "team_a_wickets",            limit: 4
      t.float   "team_a_runrate",            limit: 24
      t.float   "team_a_overs_played",       limit: 24
      t.float   "team_a_total_overs",        limit: 24
      t.string  "team_b_name",               limit: 100, null: false
      t.integer "team_b_super_over_score",   limit: 4
      t.integer "team_b_super_over_wickets", limit: 4
      t.integer "team_b_score",              limit: 4
      t.integer "team_b_wickets",            limit: 4
      t.float   "team_b_runrate",            limit: 24
      t.float   "team_b_overs_played",       limit: 24
      t.float   "team_b_total_overs",        limit: 24
      t.string  "match_result",              limit: 100, null: false
      t.string  "match_type",                limit: 100, null: false
      t.string  "winner",                    limit: 100, null: false
      t.integer "victory_margin_by_runs",    limit: 4,   null: false
      t.integer "victory_margin_by_wickets", limit: 4,   null: false
      t.integer "match_number",              limit: 4,   null: false
      t.integer "year",                      limit: 4,   null: false
      

      t.timestamps null: false
    end
  end
end
