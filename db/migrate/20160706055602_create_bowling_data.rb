class CreateBowlingData < ActiveRecord::Migration
  def change
    create_table :bowling_data do |t|
      t.string  "bowler_name",   limit: 100, null: false
      t.float   "overs",         limit: 24,  null: false
      t.integer "runs_given",    limit: 4,   null: false
      t.integer "wickets",       limit: 4,   null: false
      t.float   "economy",       limit: 24,  null: false
      t.integer "dot_balls",     limit: 4,   null: false
      t.integer "innings",       limit: 4,   null: false
      t.integer "match_number",  limit: 4,   null: false
      t.integer "year",          limit: 4,   null: false
      t.integer "match_datum_id", limit: 4

      t.timestamps null: false
    end
  end
end
