class CreateBattingData < ActiveRecord::Migration
  def change

    create_table :batting_data do |t|
      t.string  "batsmen_name",  limit: 100, null: false
      t.string  "dismissal",     limit: 100, null: false
      t.integer "runs",          limit: 4,   null: false
      t.integer "balls_faced",   limit: 4,   null: false
      t.float   "strike_rate",   limit: 24,  null: false
      t.integer "fours",         limit: 4,   null: false
      t.integer "sixes",         limit: 4,   null: false
      t.integer "year",          limit: 4,   null: false
      t.integer "match_number",  limit: 4,   null: false
      t.integer "innings",       limit: 4,   null: false
      t.integer "match_datum_id", limit: 4
      t.string  "team",          limit: 100
      

      t.timestamps null: false
    end
  end
end
