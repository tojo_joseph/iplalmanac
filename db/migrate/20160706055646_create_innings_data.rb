class CreateInningsData < ActiveRecord::Migration
  def change
    create_table :innings_data do |t|
      t.string "first_batting_team",  limit: 100
      t.string "second_batting_team", limit: 100

      t.timestamps null: false
    end
  end
end
