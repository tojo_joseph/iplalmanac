# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160706055646) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "batting_data", force: true do |t|
    t.string   "batsmen_name",   limit: 100, null: false
    t.string   "dismissal",      limit: 100
    t.integer  "runs"
    t.integer  "balls_faced"
    t.float    "strike_rate"
    t.integer  "fours"
    t.integer  "sixes"
    t.integer  "year",                       null: false
    t.integer  "match_number",               null: false
    t.integer  "innings",                    null: false
    t.integer  "match_datum_id"
    t.string   "team",           limit: 100
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "bowling_data", force: true do |t|
    t.string   "bowler_name",    limit: 100, null: false
    t.float    "overs",                      null: false
    t.integer  "runs_given",                 null: false
    t.integer  "wickets",                    null: false
    t.float    "economy",                    null: false
    t.integer  "dot_balls",                  null: false
    t.integer  "innings",                    null: false
    t.integer  "match_number",               null: false
    t.integer  "year",                       null: false
    t.integer  "match_datum_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "innings_data", force: true do |t|
    t.string   "first_batting_team",  limit: 100
    t.string   "second_batting_team", limit: 100
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  create_table "match_data", force: true do |t|
    t.string   "super_over",                limit: 10,  null: false
    t.string   "team_a_name",               limit: 100, null: false
    t.integer  "team_a_super_over_score"
    t.integer  "team_a_super_over_wickets"
    t.integer  "team_a_score"
    t.integer  "team_a_wickets"
    t.float    "team_a_runrate"
    t.float    "team_a_overs_played"
    t.float    "team_a_total_overs"
    t.string   "team_b_name",               limit: 100, null: false
    t.integer  "team_b_super_over_score"
    t.integer  "team_b_super_over_wickets"
    t.integer  "team_b_score"
    t.integer  "team_b_wickets"
    t.float    "team_b_runrate"
    t.float    "team_b_overs_played"
    t.float    "team_b_total_overs"
    t.string   "match_result",              limit: 100, null: false
    t.string   "match_type",                limit: 100, null: false
    t.string   "winner",                    limit: 100, null: false
    t.integer  "victory_margin_by_runs",                null: false
    t.integer  "victory_margin_by_wickets",             null: false
    t.integer  "match_number",                          null: false
    t.integer  "year",                                  null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

end
